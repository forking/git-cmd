import test from 'ava'
const Git = require('./')
const path = require('path')

test('repo', t => {
  const repoPath = path.resolve('./')

  const git = new Git({
    'git-dir': repoPath
  })
  return git.exec('status')
    .then(res => {
      let isRepo = typeof res !== 'object'
      t.is(isRepo, true)
    })
})
