const execa = require('execa')
const path = require('path')

function Git (options) {
  options = options || {}
  this.bin = options.bin ? options.bin : 'git'

  let gitDir = options['git-dir']
  if (gitDir) {
    let sep = path.sep === '/' ? '\\/' : path.sep
    let isGitRegex = new RegExp(sep + '\\.git$', 'g')
    if (!gitDir.match(isGitRegex)) {
      options['git-dir'] = path.join(gitDir, '.git')
    }

    if (!options['work-tree']) {
      options['work-tree'] = path.join(options['git-dir'], '..')
    }
  }
  this.args = Git.optionsToArray(options)
}

Git.prototype.exec = function (command, args) {
  args = args || []

  command = Array.isArray(command) ? command : [command]
  let rawargs = this.args.concat(command).concat(args)
  args = []

  // Get rid of nulls and undefineds.
  rawargs.forEach(item => {
    if (item != null) args.push(`${item}`)
  })

  return execa(this.bin, args).then(res => res.stdout).catch(err => new Error(err.stderr))
}

Git.optionsToArray = function (options) {
  let args = []

  Object.keys(options).forEach(k => {
    let val = options[k]

    if (k.length === 1) {
      if (val === true) {
        args.push('-' + k)
      } else if (val !== false) {
        args.push('-' + k, val)
      }
    } else {
      if (val === true) {
        args.push('--' + k)
      } else if (val !== false) {
        args.push('--' + k + '=' + val)
      }
    }
  })

  return args
}

module.exports = Git
